import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {


  num = 0;
  signUp = {
    firstName : 'Amrutha',
    lastName : 'Veeramachaneni',
    userName : 'Amruthaammu',
    age : '23',
    emailId : 'amru2u@gmail.com',
    phoneNo : '8074382311',
    address : 'abcabc',
    city : 'Hyderabad',
    state : 'Telangana',
    country : 'India',
  };
  arrayobj = [{name: 'Navya', num : 2323232},
              {name : 'Amrutha', num : 4567890},
              {name : 'Ashika', num : 1234567}];
  array = [];
  // array2 = [];
  showHide: boolean;
  constructor() { }

  ngOnInit() {

  }
  printData() {
    console.log(this.signUp);
  }
  plus() {
   this.num = this.num + 1;
  //  console.log('ok');
  }
  minus() {
   this.num = this.num - 1;
  }
  showData() {
    this.showHide = true;
    this.array = [...this.arrayobj];
    // this.showHide = !this.showHide;
    // console.log(this.array);
    }
    hideData() {
      this.showHide = false;
      this.array = [];
    }

    printChildData(data) {
      console.log(data);
    }
  }
