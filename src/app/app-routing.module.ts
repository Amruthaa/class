import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { Child2Component } from './child2/child2.component';

export const routes: Routes = [{path : '', component : AppComponent},
{path : 'parent', component : ParentComponent},
{path : 'child', component : ChildComponent},
{path : 'child2', component : Child2Component},
{path : '**', component : PageNotFoundComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
